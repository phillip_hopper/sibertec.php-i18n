<?php
/**
 * Created by PhpStorm.
 * User: Phil Hopper
 * Date: 22 Jul 2014
 * Time: 8:30 PM
 */

class i18n_helper {

	/**
	 * Determine if it is possible for the haystack to contain the needle
	 * @param string $haystack
	 * @param string $needle
	 * @return boolean
	 */
	private static function may_contain($haystack, $needle) {

		$str_len = strlen($haystack);
		$test_len = strlen($needle);
		if ($str_len < 1) return false;
		if ($test_len < 1) return false;
		if ($test_len > $str_len) return false;

		return true;
	}

	public static function beginsWith($haystack, $needle) {

		if (!self::may_contain($haystack, $needle)) return false;
		if ($haystack == $needle) return true;
		return (strpos($haystack, $needle) === 0);
	}

	public static function endsWith($haystack, $needle) {

		if (!self::may_contain($haystack, $needle)) return false;
		if ($haystack == $needle) return true;
		return substr_compare($haystack, $needle, -(strlen($needle))) === 0;
	}

	public static function contains($haystack, $needle) {

		if (!self::may_contain($haystack, $needle)) return false;
		if ($haystack == $needle) return true;
		return (strpos($haystack, $needle) !== false);
	}

	/**
	 * Combines two or more strings using DIRECTORY_SEPARATOR.
	 *
	 * @internal param string $string1 First part of the file path
	 * @internal param string $string2 Second part of the file path
	 *
	 * @return string The path properly combined
	 */
	public static function pathCombine() {

		$sep = DIRECTORY_SEPARATOR;
		$not_sep = ($sep == '/') ? '\\' : '/';

		$segments = func_get_args();
		$seg_cnt = count($segments);

		$return_val = '';

		// check for windows vs unix
		for ($i = 0; $i < $seg_cnt; $i++) {
			$segments[$i] = str_replace($not_sep, $sep, $segments[$i]);

			// remove trailing DIRECTORY_SEPARATOR from all segments
			while (self::EndsWith($segments[$i], $sep))
				$segments[$i] = substr($segments[$i], 0, strlen($segments[$i]) - 1);

			// remove leading DIRECTORY_SEPARATOR from all segments except the first
			if ($i != 0) {
				while (self::BeginsWith($segments[$i], $sep))
					$segments[$i] = substr($segments[$i], 1);
			}

			// combine the segments
			if (!empty($segments[$i])) {

				if (empty($return_val)) $return_val = $segments[$i];
				else $return_val .= $sep . $segments[$i];
			}
		}

		return $return_val;
	}

	/**
	 * Combines two or more strings using DIRECTORY_SEPARATOR and converts to the absolute path
	 *
	 * @internal param string $string1 First part of the file path
	 * @internal param string $string2 Second part of the file path
	 *
	 * @return string|bool The canonical path, or false if path not found.
	 */
	public static function realPathCombine() {

		$args = func_get_args();
		$path = call_user_func_array('self::PathCombine', $args);
		return realpath($path);
	}

}

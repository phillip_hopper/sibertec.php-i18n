<?php
/**
 * Created by PhpStorm.
 * User: Phil Hopper
 * Date: 21 Jul 2014
 * Time: 5:43 PM
 */

/* @var $i18n i18n */
$i18n = null;


function _sdate($value) {
	global $i18n;
	return $i18n->toDateStr($value);
}

function _mdate($value) {
	global $i18n;
	return $i18n->toMediumDateStr($value);
}

function _ldate($value) {
	global $i18n;
	return $i18n->toLongDateStr($value);
}

function _time($value) {
	global $i18n;
	return $i18n->toTimeStr($value);
}

/**
 * Returns $value formatted as currency
 * @param $value
 * @return string
 */
function _curr($value) {
	global $i18n;
	return $i18n->toCurrencyStr($value);
}

/**
 * Returns $value formatted as a number
 * @param $value
 * @param int $decimal_places
 * @return string
 */
function _float($value, $decimal_places = 0) {
	global $i18n;
	return $i18n->toNumberStr($value, $decimal_places);
}

function _text($key_default) {
	global $i18n;
	return $i18n->getText($key_default);
}

function _ftext($key_default) {

	global $i18n;

	$args = func_get_args();

	if (is_array($args[1]))
		$values = $args[1];
	else {
		$values = $args;
		unset($values[0]);
		$values = array_values($values);
	}

	return $i18n->getTextFormatted($key_default, $values);
}

class i18n {

	public $locale_id = '';
	private $i18n_directory;
	private $messages;
	private $formats;

	private function __construct() {

	}

	/**
	 * Initialize with information that tells i18n where to find the localized files. There are 2 possible layouts for
	 * the directory structure and file names. First, they can be in $i18n_directory, and the files are prefixed with
	 * the $locale_id like en_US.messages.json. The second option has the files in a sub-directory of $i18n_directory
	 * that is named $locale_id and the file names have no prefix, like messages.json.
	 * @param string $locale_id
	 * @param string $i18n_directory
	 */
	public static function init($locale_id, $i18n_directory) {

		global $i18n;

		$i18n = new i18n();
		$i18n->locale_id = $locale_id;
		$i18n->i18n_directory = $i18n_directory;

		$i18n->messages = $i18n->load_file('messages.json');
		$i18n->formats = $i18n->load_file('formats.json', true);
	}

	/**
	 * @param string $file_name
	 * @param bool $use_default_if_missing
	 * @return mixed|null
	 */
	private function load_file($file_name, $use_default_if_missing = false) {

		// first look for the localized files in $i18n_directory/
		$file = i18n_helper::realPathCombine($this->i18n_directory, $this->locale_id . '.' . $file_name);

		// if not found, look for them in $i18n_directory/$locale_id/
		if (!$file)
			$file = i18n_helper::realPathCombine($this->i18n_directory, $this->locale_id, $file_name);

		// should we use the default file?
		if (!$file && $use_default_if_missing)
			$file = i18n_helper::realPathCombine(dirname(__FILE__), 'default.' . $file_name);

		// if still not found, just use the default values
		if (!$file) return null;

		$contents = file_get_contents($file);

		return json_decode($contents, true);
	}

	public function toDateStr($value) {
		return self::toDateTimeStr($value, empty($this->formats) ? 'Y-m-d' : $this->formats['shortDate']);
	}

	public function toMediumDateStr($value) {
		return self::toDateTimeStr($value, empty($this->formats) ? 'd M Y' : $this->formats['mediumDate']);
	}

	public function toLongDateStr($value) {
		return self::toDateTimeStr($value, empty($this->formats) ? 'd M Y' : $this->formats['longDate']);
	}

	public function toTimeStr($value) {
		return self::toDateTimeStr($value, empty($this->formats) ? 'G:i' : $this->formats['time']);
	}

	private function toDateTimeStr($value, $format) {

		if (is_string($value))
			$value = strtotime($value);

		if (!empty($this->formats)) {

			// substitute month and day names
			// l = full day name
			// D = 3 letter day name
			// F = full month name
			// M = 3 letter month name
			$pattern = '/(?<!\\\\)[lDFM]/';
			$results = preg_match_all($pattern, $format, $matches, PREG_OFFSET_CAPTURE);
			if ($results > 0) {

				$found = $matches[0];
				for ($i = count($found)-1; $i > -1; $i--) {

					switch ($found[$i][0]) {
						case 'l':
							$format = self::charReplace($format, $found[$i][1], self::getDayName($value, false));
							break;

						case 'D':
							$format = self::charReplace($format, $found[$i][1], self::getDayName($value, true));
							break;

						case 'F':
							$format = self::charReplace($format, $found[$i][1], self::getMonthName($value, false));
							break;

						case 'M':
							$format = self::charReplace($format, $found[$i][1], self::getMonthName($value, true));
							break;
					}
				}
			}
		}

		return date($format, $value);
	}

	private static function charReplace($source, $position, $replaceWith) {

		// split utf-8 string into characters
		$a = preg_split('/(?<!^)(?!$)/u', $replaceWith);

		// escape each character
		$replaceWith = '\\' . join('\\', $a);

		return substr($source, 0, $position) . $replaceWith . substr($source, $position + 1);
	}

	private function getDayName($date, $short) {

		$i = (int) date('w', $date);
		if($short)
			return $this->formats['days3'][$i];
		else
			return $this->formats['days'][$i];
	}

	private function getMonthName($date, $short) {

		$i = ((int) date('n', $date)) - 1;
		if($short)
			return $this->formats['months3'][$i];
		else
			return $this->formats['months'][$i];
	}

	public function toCurrencyStr($value) {

		// use default formatting if no settings
		if (empty($this->formats))
			return number_format($value, 2);

		return self::formatNumber($value, $this->formats['currency']);
	}

	public function toNumberStr($value, $decimal_places = 0) {

		// use default formatting if no settings
		if (empty($this->formats))
			return number_format($value, $decimal_places);

		return self::formatNumber($value, $this->formats['number'], $decimal_places);
	}

	private static function formatNumber($value, $options, $decimal_places = 2) {

		// round
		if (isset($options['decimalPlaces']))
			$value = round($value, $options['decimalPlaces'], PHP_ROUND_HALF_UP);
		else
			$value = round($value, $decimal_places, PHP_ROUND_HALF_UP);

		// use positive or negative format
		if ($value < 0) {
			$value = abs($value);
			$neg = true;
		} else {
			$neg = false;
		}

		// put together using i18n settings
		if (isset($options['decimalPoint']))
			$dec_point = $options['decimalPoint'];
		else
			$dec_point = '.';

		if (isset($options['thousands']))
			$thousands = $options['thousands'];
		else
			$thousands = '.';

		$formatted = number_format($value, $decimal_places, $dec_point, $thousands);

		// get the correct format
		if ($neg) {
			if (isset($options['negativeFormat']))
				$format = $options['negativeFormat'];
			else
				$format = '-%s';
		} else {
			if (isset($options['positiveFormat']))
				$format = $options['positiveFormat'];
			else
				$format = '%s';
		}

		return sprintf($format, $formatted);
	}

	public function getText($key_default) {

		// if no i18n messages file, return the default
		if (empty($this->messages)) return $key_default;

		// if the messages file contain the key, return the translated value, if not empty
		if (!empty($this->messages[$key_default]))
			return $this->messages[$key_default];

		// the key was not found, or the translation was empty, return the default
		return $key_default;
	}

	/**
	 * @param string $key_default
	 * @param mixed $args An array or a comma separated list of values to substitute
	 * @return string
	 */
	public function getTextFormatted($key_default, $args) {

		if (is_array($args))
			$values = $args;
		else {
			$values = func_get_args();
			unset($values[0]); /* get rid of "$key_default" */
			$values = array_values($values);
		}

		$formats = $this->getText($key_default);

		// is this a single/plural thing
		if (is_array($formats)) {

			if ($values[0] == 1)
				$format = $formats[0];
			else
				$format = $formats[1];
		} else
			$format = $formats;

		return vsprintf($format, $values);
	}
}

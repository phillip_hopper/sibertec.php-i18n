<?php
/**
 * Created by PhpStorm.
 * User: Phil Hopper
 * Date: 24 Jul 2014
 * Time: 4:17 PM
 */

require_once dirname(dirname(__FILE__)) . '/lib/i18n.php';
require_once dirname(dirname(__FILE__)) . '/lib/i18n/helper.php';

class i18nTest extends PHPUnit_Framework_TestCase {

	/**
	* @var i18n
	*/
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {

		global $i18n;
		$i18n = null;
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {

	}

	/**
	 * @covers i18n::init
	 */
	public function testInit() {

		global $i18n;
		i18n::init('en_US', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data');

		$this->assertNotEmpty($i18n);
		$this->assertEquals('en_US', $i18n->locale_id);
	}

	/**
	 * @covers i18n->getText
	 */
	public function testGetText() {

		global $i18n;
		i18n::init('en_US', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data');

		$this->assertEquals('This is working', $i18n->getText('Verify'));
		$this->assertEquals('Missing key', $i18n->getText('Missing key'));
	}

	/**
	 * @covers i18n->getTextFormatted
	 */
	public function testGetTextFormatted() {

		global $i18n;
		i18n::init('en_US', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data');

		$this->assertEquals('There is 1 item', $i18n->getTextFormatted('There are %d items', 1));
		$this->assertEquals('There are 12 items', $i18n->getTextFormatted('There are %d items', 12));
		$this->assertEquals('one two three', $i18n->getTextFormatted('%s %s %s', 'one', 'two', 'three'));

		$args = array('one', 'two', 'three');
		$this->assertEquals('one two three', $i18n->getTextFormatted('%s %s %s', $args));
	}

	public function testToNumberStr() {

		global $i18n;
		i18n::init('fr_FR', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data');

		$this->assertEquals('1 234,56', $i18n->toNumberStr(1234.56, 2));
	}

	public function testToCurrencyStr() {

		global $i18n;
		i18n::init('fr_FR', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data');

		$this->assertEquals('1 234,56 €', $i18n->toCurrencyStr(1234.56));
		$this->assertEquals('(1 234,56 €)', $i18n->toCurrencyStr(-1234.56));
	}

	public function testToDateStr() {

		global $i18n;
		i18n::init('fr_FR', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data');
		$this->assertEquals('31/12/2013', $i18n->toDateStr(strtotime('2013-12-31')));

		i18n::init('en_US', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data');
		$this->assertEquals('12/31/2013', $i18n->toDateStr(strtotime('2013-12-31')));
	}

	public function testToMediumDateStr() {

		global $i18n;
		i18n::init('fr_FR', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data');
		$this->assertEquals('le 31 déc 2013', $i18n->toMediumDateStr(strtotime('2013-12-31')));

		i18n::init('en_US', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data');
		$this->assertEquals('31 Dec 2013', $i18n->toMediumDateStr(strtotime('2013-12-31')));
	}

	public function testToLongDateStr() {

		global $i18n;
		i18n::init('fr_FR', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data');
		$this->assertEquals('mardi, décembre 31, 2013', $i18n->toLongDateStr(strtotime('2013-12-31')));

		i18n::init('en_US', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data');
		$this->assertEquals('Tuesday, December 31, 2013', $i18n->toLongDateStr(strtotime('2013-12-31')));
	}

	public function testToTimeStr() {

		global $i18n;
		i18n::init('fr_FR', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data');
		$this->assertEquals('13:15', $i18n->toTimeStr(strtotime('2013-12-31 13:15:00')));

		i18n::init('en_US', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data');
		$this->assertEquals('1:15 PM', $i18n->toTimeStr(strtotime('2013-12-31 13:15:00')));
	}

	public function testShortcuts() {

		i18n::init('en_US', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data');

		$this->assertEquals('This is working', _text('Verify'));
		$this->assertEquals('Missing key', _text('Missing key'));

		$this->assertEquals('There is 1 item', _ftext('There are %d items', 1));
		$this->assertEquals('There are 12 items', _ftext('There are %d items', 12));
		$this->assertEquals('one two three', _ftext('%s %s %s', 'one', 'two', 'three'));

		$args = array('one', 'two', 'three');
		$this->assertEquals('one two three', _ftext('%s %s %s', $args));

		$this->assertEquals('1,234.56', _float(1234.56, 2));
		$this->assertEquals('$1,234.56', _curr(1234.56));
		$this->assertEquals('($1,234.56)', _curr(-1234.56));

		$this->assertEquals('12/31/2013', _sdate('2013-12-31'));
		$this->assertEquals('1:15 PM', _time(strtotime('2013-12-31 13:15:00')));
	}

}

Sibertec.Php-i18n is a small, easy to use PHP library that allows you to internationalize your application without requiring additional locales to be installed on the server.

# How to Use

	/**
	* Initialize with information that tells i18n where to find the localized
	* files. There are 2 possible layouts for the directory structure and file
	* names. First, they can be in $i18n_directory, and the files are prefixed
	* with the $locale_id like en_US.messages.json. The second option has the
	* files in a sub-directory of $i18n_directory that is named $locale_id and
	* the file names have no prefix, like messages.json.
	*
	* @param string $locale_id
	* @param string $i18n_directory
	*/
	i18n::init('en_US', '/your/i18n/directory/name');

	// date formats
	$shortDate = _sdate(time());
	$mediumDate = _mdate(time());
	$longDate = _ldate(time());

	// time
	$time = _time(time());

	// numbers
	$number = _float(123.456, 2);

	// currency
	$currency = _curr(123.45);

	// text
	$normal = _text('translate this string');
	$formatted = _ftext('There are %s items.', 6);




